﻿using System;
using System.Collections.Generic;
using System.Text;

class Category
{
    #region attributes
    private string _id;
    private string _description;
    #endregion

    #region constructors
    /// <summary>
    ///  Creates a blank object
    /// </summary>
    public Category()
    {
        _id = "";
        _description = "";
    }
    /// <summary>
    /// Crate object with values
    /// </summary>
    /// <param name="id"></param>
    /// <param name="description"></param>
    public Category(string id, string description)
    {
        _id = id;
        _description = description;
    }
    #endregion

    
}
