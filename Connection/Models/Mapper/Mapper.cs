﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

internal class Mapper
{
    public static Brand ToBrand(DataRow r)
    {
        Brand b = new Brand();
         b.Id = (string)r["brand_id"];
         b.Description = (string)r["brand_description"];
        return b;
    }

    public static List<Brand> ToBrandsList(DataTable table)
    {
        List<Brand> list = new List<Brand>();
        foreach (DataRow r in table.Rows)
        {
            list.Add(Mapper.ToBrand(r));
        }
        return list;
    }
}
