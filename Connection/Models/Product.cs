﻿using System;
using System.Collections.Generic;
using System.Text;

public class Product : Catalog 
{
    #region data
    private static List<Product> data = new List<Product>
    {
        new Product("1001", "Trout 5", 1500,
            new Category("CL", "Cleats"),
            new Brand("NK", "Nike")),
        new Product("1002", "A2000", 4600,
            new Category("GL", "Gloves"),
            new Brand("WL", "Wilson"))
    };
    #endregion

    #region attributes
    private double _price;
    private Category _category;
    private Brand _brand;

    public Product()
    {
        _price = price;
        _category = category;
        _brand = brand;
    }
    #endregion

    #region contructors
    public Product()
    {
        _id ="";
        _description = "";
        _price = new Category();
        _brand = new Brand();
    }
    #endregion

    #region properties
    public double Price { get => _price; set => _price = value; }
    public Category Category { get => _category; set => _category = value; }
    public Brand Brand { get => _brand; set => _brand = value; }
    #endregion
}

