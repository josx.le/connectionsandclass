﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Relational;

internal class Brand : Catalog
    {
    #region

    private static string select = "select id as brand_id, description as brands_description from brands order by description;";

    private static string selectOne = "declare @ID varchar(10) = 'NK'; select id as brand_id, description as brands_description from brands where id = \"@ID\" order by description ";

    private static string insert = "";

    #endregion
    #region data
    private static List<Brand> data = new List<Brand>
            {
                new Brand("AD", "Adidas"),
                new Brand("NK", "Nike"),
                new Brand("UA",  "Under Armour")
            };
         #endregion

        #region constructors
        ///  Creates a blank object
        public Brand()
        {
            _id = "";
            _description = "";
        }
        /// Crate object with values
        /// <param name="id"></param>
        /// <param name="description"></param>
        public Brand(string id, string description)
        {
            _id = id;
            _description = description;
        }
        #endregion

        #region intance methods
        ///  Add Bran to database
        public bool Add()
        {
            MySqlCommand command = new MySqlCommand(insert);

            command.parameters,AddWithValue("@ID", _id);
            command.parameters,AddWithValue("@DESC", _description);
    }

        /// deletes from database
        public bool Delete()
        {
            data.Remove(this);
            return true;
        }
        #endregion

        #region class methods
        public static List<Brand> Get()
        {
            MySqlCommand commmand = new MySqlCommand(select);
            return Mapper.(ToBrandsListSqlServerConnection.ExecuteQuery(commmand));
            
        }
        /// <summary>
        /// Return the brand represented by the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Bran id</returns>
        public static Brand Get(string id)
        {
            MySqlCommand = new MySqlCommand(selectOne);
            command.Parameters.AddWithValue("@ID", id);
        if (Table.Rows.Count > 0)
            return Mapper.ToBrand(Table.Rows[0]);
        else
            throw new RecordNotFoundException("Brand", id);
        }
        #endregion  
}

