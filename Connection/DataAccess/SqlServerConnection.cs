﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

internal class SqlServerConnection
    {
    #region attributes

    private static string connectionString = "Server=localhost;Database=store;User ID=kk;Password=0klama-095!;";
    private static MySqlConnection connection;

    #endregion

    #region class methods

    private static bool Open()
    {
        bool open = false;
        connection = new MySqlConnection(connectionString);
        try
        {
            connection.Open();
            open = true;
        }
        catch (MySqlException e)
        {
            Console.WriteLine(e.Message);    
        }
        catch (ArgumentException e)
        {
            Console.WriteLine(e.Message);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
        return open;
    }

    public static DataTable ExecuteQuery(MySqlCommand command)
    {
        
    }

    public static bool ExecuteNonQuery(MySqlCommand command)
    {
        bool success = false;
        if (Open()) //open connection
        {
            command.Connection = connection;
            try
            {
                command.ExecuteNonQuery();
                success = true;
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
            }
            connection.Close();
            connection.Dispose();
        }
        return success;
    }
    #endregion
}

