﻿using System;
using System.Collections.Generic;
using System.Text;

internal class Catalog
{
    #region attributes
    protected string _id;
    protected  string _description;
    #endregion

    #region properties
    public string Id { get => _id; set => _id = value; }
    public string Description { get => _description; set => _description = value; }
    #endregion

    #region intance methods
    ///  Add Brand to database
    public override string ToString()
    {
        return _id + " : " + _description;
    }
    #endregion

}