﻿using System;
using System.Collections.Generic;
using System.Text;

    internal class Brand : Catalog
    {
        #region data
        private static List<Brand> data = new List<Brand>
            {
                new Brand("AD", "Adidas"),
                new Brand("NK", "Nike"),
                new Brand("UA",  "Under Armour")
            };
         #endregion

        #region constructors
        ///  Creates a blank object
        public Brand()
        {
            _id = "";
            _description = "";
        }
        /// Crate object with values
        /// <param name="id"></param>
        /// <param name="description"></param>
        public Brand(string id, string description)
        {
            _id = id;
            _description = description;
        }
        #endregion

        #region intance methods
        ///  Add Bran to database
        public bool Add()
        {
            data.Add(this);
            return true;
        }

        /// deletes from database
        public bool Delete()
        {
            data.Remove(this);
            return true;
        }
        #endregion

        #region class methods
        public static List<Brand> Get()
        {
            return data;
        }
        /// <summary>
        /// Return the brand represented by the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Bran id</returns>
        public static Brand Get(string id)
        {
            Brand brand = new Brand();
            bool found = false;
            foreach (Brand b in data)
            {
                if (b.Id == id)
                {
                    found = true;
                    brand = b;
                }

            }
            //return brand;
            if (found)
                return brand;
            else
                throw new RecordNotFoundException("Brand", id);
        }
        #endregion  
}

