﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
     class Program
    {
        static void Main(string[] args)
        {   //add brand
            Brand nb = new Brand("RB", "REEBOOK");
            nb.Add();
            //all brands
            foreach(Brand b in Brand.Get()) 
            {
                Console.WriteLine(b);
            }
            try
            {
                Brand b = Brand.Get("RB");
                b.Delete();
            }
            catch (RecordNotFoundException ex){
                Console.WriteLine(ex.Message);

            }
            Console.WriteLine();
            // all brands 
            foreach (Brand b in Brand.Get()){
                Console.WriteLine(b);
            }
            Console.ReadLine();
        }
    }
}

